const getPrinters = () => {
        return axios.get("api/printers").then((r) => r.data)
}

const getSelectedPrinter = () => axios.get("api/selectedPrinter").then((r) => r.data)

const populateSelect = (elId, data) => {
    const select = document.getElementById(elId)
    if(select && data){
        Array.from(data).map(({deviceId, name}) => select.add(new Option(name, deviceId)))
    }
} 

const changeTitle = (data) => document.getElementById("selectedPrinter").innerHTML = "Selected printer: " + data.selectedPrinter;

const form = document.getElementById("printersForm");
  form.addEventListener("submit", (e) => {
    e.preventDefault();
    const selectedPrinter = document.getElementById("printers").value
    console.warn(selectedPrinter)
    axios
      .post("http://localhost:8000/api/changePrinter", {
        headers: {
          "Content-Type": "application/json",
        },
        data: {
            "selectedPrinter": selectedPrinter
        }
      })
      .then(() => {
        location.reload()
      })
  });