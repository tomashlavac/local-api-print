const express = require('express')
const printers = require("pdf-to-printer")
const app = express()
const port = 8000
const bodyParser = require('body-parser')
const cors = require('cors')
const path = require('path');
app.use(cors())
const jsonParser = bodyParser.json()
const fs = require('fs');
app.use(bodyParser.json({ type: 'application/*+json' }))
const {print, getPrinters} = printers
app.use(express.static(path.join(__dirname,"html")));
require('dotenv').config();
app.use(express.static(path.join(__dirname, 'html')));

app.post('/api/print', jsonParser, (req, res) => {
  fs.readFile("settings.txt", 'utf8', function (err,data) {
    if (err) {
      return console.log(err);
    }
    const base64 = req.body?.fileBase64
    fs.writeFile("sample.pdf", base64.replace(/^data:.+;base64,/, ""), 'base64', (e) => console.log(e));
    print("sample.pdf", {printer: data})
  });
    return res.sendStatus(200)
})

app.post('/api/changePrinter', jsonParser, (req, res) => {
  fs.readFile("settings.txt", 'utf8', function (err,data) {
    if (err) {
      return console.log(err);
    }
    fs.writeFile("settings.txt", req.body.data.selectedPrinter, 'utf8', function (err) {
       if (err) return console.log(err);
    });
  });
  return res.sendStatus(200)
})

app.get('/', (req, res) => {
  res.render(__dirname + '/html/index.html');
})

app.get('/api/printers', (req, res) => {
  getPrinters().then((printers) => res.send(JSON.stringify(printers)))
})

app.get('/api/selectedPrinter', (req, res) => {
  fs.readFile("settings.txt", 'utf8', function (err,data) {
    if (err) {
      return console.log(err);
    }
    res.send(JSON.stringify({selectedPrinter: data}))
  });
})


app.listen(port, () => {
  console.log(`Example app listening on port ${port}`)
})